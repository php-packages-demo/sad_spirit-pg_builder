# sad_spirit/pg_builder

Query builder for PostgreSQL backed by query parser

[![PHPPackages Rank](http://phppackages.org/p/sad_spirit/pg_builder/badge/rank.svg)](http://phppackages.org/p/sad_spirit/pg_builder)
[![PHPPackages Referenced By](http://phppackages.org/p/sad_spirit/pg_builder/badge/referenced-by.svg)](http://phppackages.org/p/sad_spirit/pg_builder)

* [sql parser](https://phppackages.org/s/sql%20parser)